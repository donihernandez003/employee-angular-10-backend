const Employee = require('../models/Employee')

module.exports = {
    get: async (req, res) => {
        const employees = await Employee.find()
        res.json(employees)
    },
    store: async (req, res) => {
        const newEmployee = new Employee(req.body)
        await newEmployee.save()

        res.send("Employee created")
    },
    show: async (req, res) => {
        const employee = await Employee.findById(req.params.id)
        res.send(employee)
    },
    update: async (req, res) => {
        await Employee.findByIdAndUpdate(req.params.id, req.body)
        res.json({ status: "Employee updated" })
    },
    delete: async (req, res) => {
        await Employee.findByIdAndDelete(req.params.id)
        res.json({ status: "Employee deleted" })
    },
}
