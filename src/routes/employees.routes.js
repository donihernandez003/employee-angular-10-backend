const { Router } = require('express')

const router = Router()

const employeesCtr = require('../controllers/employees.controller')


router.get('/', (req, res) => {
    employeesCtr.get(req, res)
})
router.post('/', (req, res) => {
    employeesCtr.store(req, res)
})
router.get('/:id', (req, res) => {
    employeesCtr.show(req, res)
})
router.put('/:id',(req, res) => {
    employeesCtr.update(req, res)
})
router.delete('/:id', (req, res) => {
    employeesCtr.delete(req, res)
})

module.exports = router
